--Drop tables
DROP TABLE AUTHOR IF EXISTS;
DROP TABLE COMMENTS IF EXISTS;
DROP TABLE NEWS IF EXISTS;
DROP TABLE TAG IF EXISTS;
DROP TABLE NEWS_TAG IF EXISTS;
DROP TABLE NEWS_AUTHOR IF EXISTS;
DROP TABLE ROLES IF EXISTS;
DROP TABLE USERS IF EXISTS;

--Create tables
--Create table AUTHOR
CREATE TABLE AUTHOR(
  AUTHOR_ID NUMBER(20) NOT NULL,
  AUTHOR_NAME VARCHAR2(30) NOT NULL,
  CONSTRAINT AUTHOR_PK PRIMARY KEY 
  (AUTHOR_ID)
  ENABLE
);

--Create table NEWS
CREATE TABLE NEWS(
  NEWS_ID NUMBER(20) NOT NULL,
  TITLE VARCHAR2(30) NOT NULL,
  SHORT_TEXT VARCHAR2(100) NOT NULL,
  FULL_TEXT VARCHAR2(2000) NOT NULL,
  CREATION_DATE TIMESTAMP NOT NULL,
  MODIFICATION_DATE DATE NOT NULL,
  CONSTRAINT NEWS_PK PRIMARY KEY 
  (NEWS_ID)
  ENABLE
);

--Create table COMMENTS
CREATE TABLE COMMENTS(
  COMMENT_ID NUMBER(20) NOT NULL,
  NEWS_ID NUMBER(20) NOT NULL,
  COMMENT_TEXT VARCHAR2(100) NOT NULL,
  CREATION_DATE TIMESTAMP NOT NULL,
  CONSTRAINT COMMENT_PK PRIMARY KEY 
  (COMMENT_ID)
  ENABLE
);

--Create table TAG
CREATE TABLE TAG(
  TAG_ID NUMBER(20) NOT NULL,
  TAG_NAME VARCHAR2(30) NOT NULL,
  CONSTRAINT TAG_PK PRIMARY KEY 
  (TAG_ID)
  ENABLE
);

--Create table NEWS_TAG
CREATE TABLE NEWS_TAG(
  NEWS_ID NUMBER(20) NOT NULL,
  TAG_ID NUMBER(20) NOT NULL
);

--Create table NEWS_AUTHOR
CREATE TABLE NEWS_AUTHOR(
  NEWS_ID NUMBER(20) NOT NULL,
  AUTHOR_ID NUMBER(20) NOT NULL
);

--Create table USERS
CREATE TABLE USERS(
  USER_ID NUMBER(20) NOT NULL,
  USER_NAME VARCHAR2(50) NOT NULL,
  LOGIN VARCHAR2(30) NOT NULL,
  PASSWORD VARCHAR2(30) NOT NULL,
  EXPIRED TIMESTAMP NOT NULL,
  CONSTRAINT USER_PK PRIMARY KEY 
  (USER_ID)
  ENABLE
);

--Create table ROLES
CREATE TABLE ROLES(
  USER_ID NUMBER(20) NOT NULL,
  ROLE_NAME VARCHAR2(50) NOT NULL
);

--DROP SEQUENCE
DROP SEQUENCE SEQ_NEWS_ID IF EXISTS;
DROP SEQUENCE SEQ_AUTHOR_ID IF EXISTS;
DROP SEQUENCE SEQ_TAG_ID IF EXISTS;
DROP SEQUENCE SEQ_COMMENTS_ID IF EXISTS;
DROP SEQUENCE SEQ_USERS_ID IF EXISTS;

--CREATE SEQUENCE FOR ALL TABLE
CREATE SEQUENCE SEQ_NEWS_ID START WITH 50 INCREMENT BY 1;
CREATE SEQUENCE SEQ_AUTHOR_ID START WITH 50 INCREMENT BY 1;
CREATE SEQUENCE SEQ_TAG_ID START WITH 50 INCREMENT BY 1;
CREATE SEQUENCE SEQ_COMMENTS_ID START WITH 50 INCREMENT BY 1;
CREATE SEQUENCE SEQ_USERS_ID START WITH 50 INCREMENT BY 1;

