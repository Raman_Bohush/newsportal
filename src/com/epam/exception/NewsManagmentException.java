package com.epam.exception;

/**
 * Basic News Management exception.
 * @author Raman_Bohush
 *
 */
public class NewsManagmentException extends Exception{

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	public NewsManagmentException(String message, Throwable cause) {
		super(message, cause);
	}

	public NewsManagmentException(String message) {
		super(message);
	}

	public NewsManagmentException(Throwable cause) {
		super(cause);
	}	
}
