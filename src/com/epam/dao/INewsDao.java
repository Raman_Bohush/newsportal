package com.epam.dao;

import java.util.List;

import com.epam.dao.exception.DaoException;
import com.epam.domain.Author;
import com.epam.domain.News;
import com.epam.domain.Tag;

/**
 * News Data Access Object interface.
 * Provides additional operations with news persistent object.
 * @author Raman_Bohush
 *
 */
public interface INewsDao extends IGenericDao<News>{
	
	/**
	 * Finds news persistent object by author.
	 * @param author the author
	 * @return list of found news
	 * @throws DaoException
	 */
	public List<News> searchNewsByAuthor(Author author) throws DaoException;
	
	/**
	 * Finds news persistent object by tag.
	 * @param tag the tag
	 * @return list of found news
	 * @throws DaoException
	 */
	public List<News> searchNewsByTag(Tag tag) throws DaoException;
	
	/**
	 * Gets sorted list of news by comments.
	 * @return sorted list of news
	 * @throws DaoException
	 */
	public List<News> getSortedNews() throws DaoException;
	
	/**
	 * Binds author with the news. Add author for news.
	 * @param authorId the author id
	 * @param newsId the news id
	 * @throws DaoException
	 */
	public void addAuthorForNews(Long authorId, Long newsId) throws DaoException;
	
	/**
	 * Binds tag with the news. Add tag for news.
	 * @param newsId the news id
	 * @param tagId the tag id
	 * @throws DaoException
	 */
	public void addTagForNews(Long newsId, Long tagId) throws DaoException;
} 
