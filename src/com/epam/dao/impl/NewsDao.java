package com.epam.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.dao.INewsDao;
import com.epam.dao.exception.DaoException;
import com.epam.domain.Author;
import com.epam.domain.News;
import com.epam.domain.Tag;

/**
 * News Repository
 * Implements all operations with news object.
 * @author Roma
 *
 */
@Repository
public class NewsDao implements INewsDao{
	
	/**
	 * Receives a connection.
	 */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * SQL request for create record.
	 */
	private final static String CREATE_SQL = 
			"INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) "
			+ "VALUES(SEQ_NEWS_ID.nextVal, ?, ?, ?, ?, ?)";
	
	/**
	 * SQL request for read last added record.
	 */
	private final static String READ_LAST_SQL = 
			"SELECT SEQ_NEWS_ID.CURRVAL "
			+ "FROM DUAL";
	
	/**
	 * SQL request for read record by id.
	 */
	private final static String READ_BY_ID_SQL = 
			"SELECT news_id, title, short_text, full_text, creation_date, modification_date "
			+ "FROM news "
			+ "WHERE news_id=?";
	
	/**
	 * SQL request for update record.
	 */
	private final static String UPDATE_SQL = 
			"UPDATE news "
			+ "SET title=?, short_text=?, full_text=?, creation_date=?, modification_date=? "
			+ "WHERE news_id=?";
	
	/**
	 * SQL request for delete record.
	 */
	private final static String DELETE_SQL = 
			"DELETE FROM news "
			+ "WHERE news_id=?";
	
	/**
	 * SQL request for read all records.
	 */
	private final static String READ_ALL_SQL = 
			"SELECT news_id, title, short_text, full_text, creation_date, modification_date "
			+ "FROM news";
	
	/**
	 * SQL request for search records by author.
	 */
	private final static String SEARCH_NEWS_BY_AUTHOR_SQL = 
			"SELECT news.news_id, news.title, news.short_text, news.full_text, news.creation_date, news.modification_date "
			+ "FROM news JOIN news_author "
			+ "ON news.news_id=news_author.news_id "
			+ "WHERE news_author.author_id=?";
	
	/**
	 * SQL request for search records by tag.
	 */
	private final static String SEARCH_NEWS_BY_TAG_SQL = 
			"SELECT news.news_id, news.title, news.short_text, news.full_text, news.creation_date, news.modification_date "
			+ "FROM news JOIN news_tag "
			+ "ON news.news_id=news_tag.news_id "
			+ "WHERE news_tag.tag_id=?";
	
	/**
	 * SQL request for receiving sorted records.
	 */
	private final static String GET_SORTED_NEWS_SQL = 
			"SELECT news.news_id, news.title, news.short_text, news.full_text, news.creation_date, news.modification_date, COUNT(comments.comment_id) AS comm "
			+ "FROM news LEFT JOIN comments "
			+ "ON news.news_id=comments.news_id "
			+ "GROUP BY news.news_id, news.title, news.short_text, news.full_text, news.creation_date, news.modification_date "
			+ "ORDER BY comm DESC";
	
	/**
	 * SQL request for create record in the News_Author table.
	 */
	private final static String ADD_AUTHOR_FOR_NEWS_SQL = 
			"INSERT INTO news_author(news_id, author_id) "
			+ "VALUES(?, ?)";
	
	/**
	 * SQL request for create record in the News_Tag table.
	 */
	private final static String ADD_TAG_FOR_NEWS_SQL = 
			"INSERT INTO news_tag(news_id, tag_id) "
			+ "VALUES(?, ?)";
	
	private NewsDao(){}

	@Override
	public long create(News news) throws DaoException {
		
		ResultSet rs = null;
		long id = 0;
		try(Connection con = dataSource.getConnection(); 
				PreparedStatement ps = con.prepareStatement(CREATE_SQL);
				Statement statement = con.createStatement()){
			
			ps.setString(1,news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			ps.setDate(5, new Date(news.getModificationDate().getTime()));
			ps.executeUpdate();
			
			rs = statement.executeQuery(READ_LAST_SQL);
			if(rs.next()){
				id = rs.getInt(1);
			}
		}catch(SQLException e){
			throw new DaoException("SQL error in the create operation.", e);
		}finally{
			if(rs != null){
				try {
					rs.close();
				} catch (SQLException e) {
					throw new DaoException("Error closing.", e);
				}
			}
		}
		return id;
	}

	@Override
	public News read(Long newsId) throws DaoException {

		ResultSet rs = null;
		News news = null;
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(READ_BY_ID_SQL)){
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if(rs.next()){	
				news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
			}
		}catch (SQLException e) {
			throw new DaoException("SQL error in the read operation.", e);
		}finally{
			if(rs != null){
					try {
						rs.close();
					} catch (SQLException e) {
						throw new DaoException("Error closing.", e);
					}
			}
		}
		return news;
	}

	@Override
	public void update(News news) throws DaoException {

		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(UPDATE_SQL)){
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
			ps.setDate(5, new Date(news.getModificationDate().getTime()));
			ps.setLong(6, news.getNewsId());
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the update operation.", e);
		}
		
	}

	@Override
	public void delete(Long id) throws DaoException {
		
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(DELETE_SQL)){
			ps.setLong(1, id);	
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the delete operation.", e);
		}
		
	}

	@Override
	public List<News> readAll() throws DaoException {
		
		List<News> newsList = new ArrayList<News>();
		News news = null;
		
		try(Connection con = dataSource.getConnection();
				Statement statement = con.createStatement();
				ResultSet rs = statement.executeQuery(READ_ALL_SQL)){
			while(rs.next()){
				news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
				newsList.add(news);
			}
		}catch (SQLException e) {
			throw new DaoException("SQL error in the read  all operation.", e);
		}
		return newsList;
	}

	@Override
	public List<News> searchNewsByAuthor(Author author) throws DaoException {
		
		List<News> newsList = new ArrayList<>();
		ResultSet rs = null;
		News news = null;
		
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(SEARCH_NEWS_BY_AUTHOR_SQL)){
			ps.setLong(1, author.getAuthorId());
			rs = ps.executeQuery();
			while(rs.next()){
				news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
				newsList.add(news);
			}
		}catch(SQLException e) {
			throw new DaoException("SQL error in the search news by author operation.", e);
		}
		return newsList;
	}

	@Override
	public List<News> searchNewsByTag(Tag tag) throws DaoException {
		
		List<News> newsList = new ArrayList<>();
		ResultSet rs = null;
		News news = null;
		
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(SEARCH_NEWS_BY_TAG_SQL)){
			ps.setLong(1, tag.getTagId());
			rs = ps.executeQuery();
			while(rs.next()){
				news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
				newsList.add(news);
			}
		}catch(SQLException e) {
			throw new DaoException("SQL error in the search news by tag operation.", e);
		}
		return newsList;
	}

	@Override
	public List<News> getSortedNews() throws DaoException {
		
		List<News> sortedNewsList = new ArrayList<News>();
		News news = null;
		
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(GET_SORTED_NEWS_SQL);
				ResultSet rs = ps.executeQuery()){
			while(rs.next()){
				news = new News();
				news.setNewsId(rs.getLong(1));
				news.setTitle(rs.getString(2));
				news.setShortText(rs.getString(3));
				news.setFullText(rs.getString(4));
				news.setCreationDate(rs.getTimestamp(5));
				news.setModificationDate(rs.getDate(6));
				sortedNewsList.add(news);
			}
		}catch (SQLException e) {
			throw new DaoException("SQL error in the get sorted news operation.", e);
		}
		return sortedNewsList;
	}

	@Override
	public void addAuthorForNews(Long newsId, Long authorId) throws DaoException {
		
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(ADD_AUTHOR_FOR_NEWS_SQL)){
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the add author for news operation.", e);
		}
		
	}

	@Override
	public void addTagForNews(Long newsId, Long tagId) throws DaoException {
		
		try(Connection con = dataSource.getConnection(); 
				PreparedStatement ps = con.prepareStatement(ADD_TAG_FOR_NEWS_SQL)){
			ps.setLong(1, newsId);
			ps.setLong(2, tagId);
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the add tags for news operation.", e);
		}
	}
	
	
}
