package com.epam.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.epam.dao.ITagDao;
import com.epam.dao.exception.DaoException;
import com.epam.domain.Tag;

/**
 * Tag Repository
 * Implements all operations with tag object.
 * @author Roma
 *
 */
@Repository
public class TagDao implements ITagDao{
	
	/**
	 * Receives a connection.
	 */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * SQL request for create record.
	 */
	private final static String CREATE_SQL = 
			"INSERT INTO tag (tag_id, tag_name) "
			+ "VALUES(SEQ_TAG_ID.nextVal, ?)";
	
	/**
	 * SQL request for read last added record.
	 */
	private final static String READ_LAST_SQL = 
			"SELECT SEQ_TAG_ID.CURRVAL "
			+ "FROM DUAL";
	
	/**
	 * SQL request for read record by id.
	 */
	private final static String READ_BY_ID_SQL = 
			"SELECT tag_id, tag_name "
			+ "FROM tag "
			+ "WHERE tag_id=?";
	
	/**
	 * SQL request for update record.
	 */
	private final static String UPDATE_SQL = 
			"UPDATE tag "
			+ "SET tag_name=? "
			+ "WHERE tag_id=?";
	
	/**
	 * SQL request for delete record.
	 */
	private final static String DELETE_SQL = 
			"DELETE FROM tag "
			+ "WHERE tag_id=?";
	
	/**
	 * SQL request for read all records.
	 */
	private final static String READ_ALL_SQL = 
			"SELECT tag_id, tag_name "
			+ "FROM tag";

	private TagDao(){}

	@Override
	public long create(Tag tag) throws DaoException {
		
		ResultSet rs = null;
		long id = 0;
		try(Connection con = dataSource.getConnection(); 
				PreparedStatement ps = con.prepareStatement(CREATE_SQL);
				Statement statement = con.createStatement()){
			
			ps.setString(1, tag.getTagName());
			ps.executeUpdate();
			
			rs = statement.executeQuery(READ_LAST_SQL);
			if(rs.next()){
				id = rs.getInt(1);
			}
		}catch(SQLException e){
			throw new DaoException("SQL error in the create operation.", e);
		}finally{
			if(rs != null){
				try {
					rs.close();
				} catch (SQLException e) {
					throw new DaoException("Error closing.", e);
				}
			}
		}
		return id;
	}
 
	@Override
	public Tag read(Long tagId) throws DaoException {
		
		ResultSet rs = null;
		Tag tag = null;
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(READ_BY_ID_SQL)){
			ps.setLong(1, tagId);
			rs = ps.executeQuery();
			if(rs.next()){		
				tag = new Tag();
				tag.setTagId(rs.getLong(1));
				tag.setTagName(rs.getString(2));
			}			
		}catch (SQLException e) {
			throw new DaoException("SQL error in the read operation.", e);
		}finally{
			if(rs != null){
					try {
						rs.close();
					} catch (SQLException e) {
						throw new DaoException("Error closing.", e);
					}
			}
		}
		return tag;
	}

	@Override
	public void update(Tag tag) throws DaoException {
		
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(UPDATE_SQL)){
			ps.setString(1, tag.getTagName());
			ps.setLong(2, tag.getTagId());
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the update operation.", e);
		}
	}

	@Override
	public void delete(Long id) throws DaoException {

		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(DELETE_SQL)){
			ps.setLong(1, id);
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the delete operation.", e);
		}	
	}

	@Override
	public List<Tag> readAll() throws DaoException {
		
		List<Tag> tagsList = new ArrayList<Tag>();
		Tag tag = null;
		
		try(Connection con = dataSource.getConnection();
				Statement statement = con.createStatement();
				ResultSet rs = statement.executeQuery(READ_ALL_SQL)){
			while(rs.next()){
				tag = new Tag();
				tag.setTagId(rs.getLong(1));
				tag.setTagName(rs.getString(2));
				tagsList.add(tag);
			}
		}catch (SQLException e) {
			throw new DaoException("SQL error in the read all operation.", e);
		}
		return tagsList;
	}

	
	
}
