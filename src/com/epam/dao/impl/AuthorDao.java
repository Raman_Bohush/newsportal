package com.epam.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.epam.dao.IAuthorDao;
import com.epam.dao.exception.DaoException;
import com.epam.domain.Author;

/**
 * Author Repository
 * Implements all operations with author object.
 * @author Roma
 *
 */
@Repository
public class AuthorDao implements IAuthorDao{
	
	/**
	 * Receives a connection.
	 */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * SQL request for create record.
	 */
	private final static String CREATE_SQL = 
			"INSERT INTO author (author_id, author_name) "
			+ "VALUES(SEQ_AUTHOR_ID.nextVal, ?)";
	
	/**
	 * SQL request for read last added record.
	 */
	private final static String READ_LAST_SQL = 
			"SELECT SEQ_AUTHOR_ID.CURRVAL "
			+ "FROM DUAL";
	
	/**
	 * SQL request for read record by id.
	 */
	private final static String READ_BY_ID_SQL = 
			"SELECT author_id, author_name "
			+ "FROM author "
			+ "WHERE author_id=?";
	
	/**
	 * SQL request for update record.
	 */
	private final static String UPDATE_SQL = 
			"UPDATE author "
			+ "SET author_name=? "
			+ "WHERE author_id=?";
	
	/**
	 * SQL request for delete record.
	 */
	private final static String DELETE_SQL = 
			"DELETE FROM author "
			+ "WHERE author_id=?";
	
	/**
	 * SQL request for read all records.
	 */
	private final static String READ_ALL_SQL = 
			"SELECT author_id, author_name "
			+ "FROM author";
	
	private AuthorDao() {}

	@Override
	public long create(Author author) throws DaoException {
		
		ResultSet rs = null;
		long id = 0;
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(CREATE_SQL);
				Statement statement = con.createStatement()){
			
			ps.setString(1, author.getName());
			ps.executeUpdate();
			
			rs = statement.executeQuery(READ_LAST_SQL);
			if(rs.next()){
				id = rs.getInt(1);
			}
		}catch(SQLException e){
			throw new DaoException("SQL error in the create operation.", e);
		}finally{
			if(rs != null){
				try {
					rs.close();
				} catch (SQLException e) {
					throw new DaoException("Error closing.", e);
				}
			}
		}
		return id;
	}

	@Override
	public Author read(Long authorId) throws DaoException {
		
		ResultSet rs = null;
		Author author = null;
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(READ_BY_ID_SQL)){
			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			if(rs.next()){	
				author = new Author();
				author.setAuthorId(rs.getLong(1));
				author.setName(rs.getString(2));
			}			
		}catch (SQLException e) {
			throw new DaoException("SQL error in the read operation.", e);
		}finally{
			if(rs != null){
					try {
						rs.close();
					} catch (SQLException e) {
						throw new DaoException("Error closing.", e);
					}
			}
		}
		return author;
	}

	@Override
	public void update(Author author) throws DaoException {
		
		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(UPDATE_SQL)){
			ps.setString(1, author.getName());
			ps.setLong(2, author.getAuthorId());
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the update operation.", e);
		}
	}
	
	@Override
	public void delete(Long id) throws DaoException {

		try(Connection con = dataSource.getConnection();
				PreparedStatement ps = con.prepareStatement(DELETE_SQL)){
			ps.setLong(1, id);
			ps.executeUpdate();
		}catch (SQLException e) {
			throw new DaoException("SQL error in the delete operation.", e);
		}	
	}

	@Override
	public List<Author> readAll() throws DaoException {
		
		List<Author> authorsList = new ArrayList<Author>();
		Author author = null;
		
		try(Connection con = dataSource.getConnection();
				Statement statement = con.createStatement();
				ResultSet rs = statement.executeQuery(READ_ALL_SQL)){
			while(rs.next()){
				author = new Author();
				author.setAuthorId(rs.getLong(1));
				author.setName(rs.getString(2));
				authorsList.add(author);
			}
		}catch (SQLException e) {
			throw new DaoException("SQL error in the read all operation.", e);
		}
		return authorsList;
	}
	
}
