package com.epam.dao.exception;

import com.epam.exception.NewsManagmentException;

/**
 * Exception in the Data Access Object layer.
 * @author Raman_Bohush
 *
 */
public class DaoException extends NewsManagmentException{

	/**
	 * Serial version ID.
	 */
	private static final long serialVersionUID = 1L;

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}
}
