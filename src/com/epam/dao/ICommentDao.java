package com.epam.dao;

import com.epam.domain.Comment;

/**
 * Comment Data Access Object interface.
 * Provides additional operations with comment persistent object.
 * @author Raman_Bohush
 *
 */
public interface ICommentDao extends IGenericDao<Comment>{}
