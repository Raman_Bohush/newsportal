package com.epam.dao;

import com.epam.domain.Author;

/**
 * Author Data Access Object interface.
 * Provides additional operations with author persistent object.
 * @author Raman_Bohush
 *
 */
public interface IAuthorDao extends IGenericDao<Author>{}
