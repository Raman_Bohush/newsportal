package com.epam.domain;

/**
 * Describes binding author with news.
 * Describes the properties of the table News_Author.
 * @author Raman_Bohush
 *
 */
public class NewsAuthor {

	/**
	 * Property - news ID (foreign key).
	 */
	private Long newsId;
	
	/**
	 * Property - author ID (foreign key).
	 */
	private Long authorId;
	
	public NewsAuthor(){}

	public NewsAuthor(Long newsId, Long authorId) {
		
		this.newsId = newsId;
		this.authorId = authorId;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (authorId ^ (authorId >>> 32));
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsAuthor other = (NewsAuthor) obj;
		if (authorId != other.authorId)
			return false;
		if (newsId != other.newsId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsAuthor [newsId=" + newsId + ", authorId=" + authorId + "]";
	}
		
}
