package com.epam.domain;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Describes the properties of the table Comments.
 * @author Raman_Bohush
 *
 */
public class Comment {
	
	/**
	 * Property - comment ID (primary key).
	 */
	private Long commentId;
	
	/**
	 * Property - text comment.
	 */
	private String commentText;
	
	/**
	 * Property - creation date comment.
	 */
	private Timestamp creationDate;
	
	/**
	 * Property - news ID (foreign key).
	 */
	private Long newsId;
	
	public Comment(){}

	public Comment(Long commentId, String commentText, Timestamp creationDate, 
			Long newsId) {		
		this.commentId = commentId;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.newsId = newsId;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}	

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (commentId ^ (commentId >>> 32));
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentId != other.commentId)
			return false;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId != other.newsId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", commentText="
				+ commentText + ", creationDate=" + creationDate + ", newsId="
				+ newsId + "]";
	}		
	
}
