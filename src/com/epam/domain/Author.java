package com.epam.domain;

/**
 * Describes the properties of the table Author.
 * @author Raman_Bohush
 *
 */
public class Author {

	/**
	 * Property - author ID (primary key).
	 */
	private Long authorId;
	
	/**
	 * Property - author's name.
	 */
	private String name;
	
	public Author(){}

	public Author(Long authorId, String name) {
		this.authorId = authorId;
		this.name = name;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {

		String a = "abc";
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("ID: " + authorId).append(", name: " + name);
		return stringBuilder.toString();
		
	}
	
	
}
