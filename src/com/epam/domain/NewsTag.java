package com.epam.domain;

/**
 * Describes binding tag with news.
 * Describes the properties of the table News_Tag.
 * @author Raman_Bohush
 *
 */
public class NewsTag {

	/**
	 * Property - news ID (foreign key).
	 */
	private Long newsId;
	
	/**
	 * Property - tag ID (foreign key).
	 */
	private Long tagId;
	
	public NewsTag(){}

	public NewsTag(Long newsId, Long tagId) {

		this.newsId = newsId;
		this.tagId = tagId;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (newsId ^ (newsId >>> 32));
		result = prime * result + (int) (tagId ^ (tagId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsTag other = (NewsTag) obj;
		if (newsId != other.newsId)
			return false;
		if (tagId != other.tagId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsTag [newsId=" + newsId + ", tagId=" + tagId + "]";
	}
		
}
