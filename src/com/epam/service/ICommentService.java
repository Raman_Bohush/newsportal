package com.epam.service;

import com.epam.domain.Comment;

/**
 * Comment Service interface.
 * Set of additional operations for comment object 
 * and coordinates the application's response in each operation.
 * @author Roma
 *
 */
public interface ICommentService extends IGenericService<Comment>{}
