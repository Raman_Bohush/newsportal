package com.epam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.domain.Author;
import com.epam.domain.News;
import com.epam.domain.Tag;
import com.epam.service.INewsManagementService;
import com.epam.service.exception.ServiceException;

@Service
public class NewsManagementService implements INewsManagementService{
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private NewsService newsService;
	
	@Autowired
	private CommentService commentService;
	
	@Autowired
	private TagService tagService;
	
	@Transactional
	@Override
	public void saveNews(News news, Author author, List<Tag> tagsList) throws ServiceException {
		
		newsService.create(news);
		newsService.addAuthorForNews(news.getNewsId(), author.getAuthorId());
		for (Tag tag : tagsList) {
			newsService.addTagForNews(news.getNewsId(), tag.getTagId());
		}				
	}

	
}
