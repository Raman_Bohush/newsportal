package com.epam.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dao.exception.DaoException;
import com.epam.dao.impl.CommentDao;
import com.epam.domain.Comment;
import com.epam.service.ICommentService;
import com.epam.service.exception.ServiceException;

@Service
public class CommentService implements ICommentService{
	
	private final static Logger logger = Logger.getLogger(CommentService.class);
	
	@Autowired
	private CommentDao commentDao;

	@Override
	public long create(Comment comment) throws ServiceException {
		
		long id = 0;
		
		if(comment == null){
			logger.info("Comment is empty.");
			throw new ServiceException("Comment is empty.");
		}
		
		try {
			id = commentDao.create(comment);
		} catch (DaoException e) {
			logger.error("Can not create comment.", e);
			throw new ServiceException("Can not create comment.", e);
		}
		return id;
		
	}

	@Override
	public Comment read(Long id) throws ServiceException {
		
		Comment comment = null;
		try {
			comment = commentDao.read(id);
		} catch (DaoException e) {
			logger.error("Can not read comment", e);
			throw new ServiceException("Can not read comment", e);
		}
		return comment;
	}

	@Override
	public void update(Comment comment) throws ServiceException {
		
		if(comment == null){
			logger.info("Comment is empty.");
			throw new ServiceException("Comment is empty.");
		}
		
		try {
			commentDao.update(comment);
		} catch (DaoException e) {
			logger.error("Can not update comment", e);
			throw new ServiceException("Can not update comment", e);
		}
		
	}

	@Override
	public void delete(Long id) throws ServiceException {
		
		try {
			commentDao.delete(id);
		} catch (DaoException e) {
			logger.error("Can not delete comment", e);
			throw new ServiceException("Can not delete comment", e);
		}
			
		
	}

	@Override
	public List<Comment> readAll() throws ServiceException {
		
		List<Comment> commentsList;
		try {
			commentsList = commentDao.readAll();
		} catch (DaoException e) {
			logger.error("Can not read all comments.", e);
			throw new ServiceException("Can not read all comments.", e);
		}
		return commentsList;
	}

}
