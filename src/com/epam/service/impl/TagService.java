package com.epam.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.dao.exception.DaoException;
import com.epam.dao.impl.TagDao;
import com.epam.domain.Tag;
import com.epam.service.ITagService;
import com.epam.service.exception.ServiceException;

@Service
public class TagService implements ITagService{
	
	private final static Logger logger = Logger.getLogger(TagService.class);
	
	@Autowired
	private TagDao tagDao;

	@Override
	public long create(Tag tag) throws ServiceException {
		
		long id = 0;
		
		if(tag == null){
			logger.info("Tag is empty.");
			throw new ServiceException("Tag is empty.");
		}
		
		try {
			id = tagDao.create(tag);
		} catch (DaoException e) {
			logger.error("Can not create tag", e);
			throw new ServiceException("Can not create tag", e);
		}
		return id;
		
	}

	@Override
	public Tag read(Long id) throws ServiceException {
		
		Tag tag = null;
		try {
			tag = tagDao.read(id);
		} catch (DaoException e) {
			logger.error("Can not read tag", e);
			throw new ServiceException("Can not read tag", e);
		}
		return tag;
	}

	@Override
	public void update(Tag tag) throws ServiceException {
		
		if(tag == null){
			logger.info("Tag is empty.");
			throw new ServiceException("Tag is empty.");
		}
		
		try {
			tagDao.update(tag);
		} catch (DaoException e) {
			logger.error("Can not update tag", e);
			throw new ServiceException("Can not update tag", e);
		}
	}

	@Override
	public void delete(Long id) throws ServiceException {

		try {
			tagDao.delete(id);
		} catch (DaoException e) {
			logger.error("Can not delete tag", e);
			throw new ServiceException("Can not delete tag", e);
		}
		
	}

	@Override
	public List<Tag> readAll() throws ServiceException {
		
		List<Tag> tagsList;
		try {
			tagsList = tagDao.readAll();
		} catch (DaoException e) {
			logger.error("Can not read all tags.", e);
			throw new ServiceException("Can not read all tags.", e);
		}
		return tagsList;
	}
	
}
