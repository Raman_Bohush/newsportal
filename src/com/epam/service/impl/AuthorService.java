package com.epam.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dao.exception.DaoException;
import com.epam.dao.impl.AuthorDao;
import com.epam.domain.Author;
import com.epam.service.IAuthorService;
import com.epam.service.exception.ServiceException;

@Service
public class AuthorService implements IAuthorService{
	
	private final static Logger logger = Logger.getLogger(AuthorService.class);

	@Autowired
	private AuthorDao authorDao;
	
	@Override
	public long create(Author author) throws ServiceException {
		
		long id = 0;
		
		if(author == null){
			logger.info("Author is empty.");
			throw new ServiceException("Author is empty.");
		}
		
		try {
			id = authorDao.create(author);
		} catch (DaoException e) {
			logger.error("Can not create author.", e);
			throw new ServiceException("Can not create author.", e);
		}
		return id;
		
		
	}

	@Override
	public Author read(Long id) throws ServiceException {
		
		Author author = null;
		try {
			author = authorDao.read(id);
		} catch (DaoException e) {
			logger.error("Can not read author.", e);
			throw new ServiceException("Can not read author.", e);
		}
		return author;
	}

	@Override
	public void update(Author author) throws ServiceException {
		
		if(author == null){
			logger.info("Author is empty.");
			throw new ServiceException("Author is empty.");
		}
		
		try {
			authorDao.update(author);
		} catch (DaoException e) {
			logger.error("Can not update author.", e);
			throw new ServiceException("Can not update author.", e);
		}
		
	}

	@Override
	public void delete(Long id) throws ServiceException {
		
		try {
			authorDao.delete(id);
		} catch (DaoException e) {
			logger.error("Can not delete author.", e);
			throw new ServiceException("Can not delete author.", e);
		}
		
	}

	@Override
	public List<Author> readAll() throws ServiceException {
		
		List<Author> authorsList;
		try {
			authorsList = authorDao.readAll();
		} catch (DaoException e) {
			logger.error("Can not read all authors.", e);
			throw new ServiceException("Can not read all authors.", e);
		}
		return authorsList;
	}
	
}
