package com.epam.service;

import com.epam.domain.Author;

/**
 * Author Service interface.
 * Set of additional operations for author object 
 * and coordinates the application's response in each operation.
 * @author Roma
 *
 */
public interface IAuthorService extends IGenericService<Author>{}
