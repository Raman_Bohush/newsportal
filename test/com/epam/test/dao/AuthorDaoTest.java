package com.epam.test.dao;

import java.io.File;
import java.util.List;
import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import com.epam.dao.impl.AuthorDao;
import com.epam.domain.Author;

/**
 * Tests Author Data Access Object using DBUnit framework.
 * @author Roma
 *
 */
public class AuthorDaoTest extends DBUnitConfig{

	private final static String AUTHOR_TABLE = "author";
	
	/**
	 * Author Data Access Object.
	 */
	@Autowired
	private AuthorDao authorDao;
	
	@Before
	@Override
	public void setUp() throws Exception {
		
		super.setUp();
		databaseDataset = new FlatXmlDataSetBuilder().build(new File(DATABASE_DATASET_PATH));
		tester.setDataSet(databaseDataset);
        tester.onSetup();
	}
	
	/**
	 * Tests create operation.
	 * @throws Exception
	 */
	@Test
	public void createAuthor() throws Exception{
		
		Author author = new Author();
		author.setName("Sveta");
		
		long id = authorDao.create(author);
		Assert.assertEquals(author.getName(), authorDao.read(id).getName());
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(AUTHOR_TABLE);		
        Assert.assertEquals(4, actualTable.getRowCount());
		
	}
	
	/**
	 * Tests read by id operation.
	 * @throws Exception
	 */
	@Test
	public void readAuthorById() throws Exception{
		
		Author author = authorDao.read(2L);
		
		Assert.assertEquals("Masha", author.getName());		
	}
	
	/**
	 * Tests update operation.
	 * @throws Exception
	 */
	@Test
	public void updateAuthor() throws Exception{
		
		Author author = new Author();
		author.setAuthorId(1L);
		author.setName("Roman Bogush");
		
		authorDao.update(author);
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(AUTHOR_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_UPDATE_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(AUTHOR_TABLE);
		
		Assertion.assertEquals(expectedlTable, actualTable);
		
	}
	
	/**
	 * Tests delete operation.
	 * @throws Exception
	 */
	@Test
	public void deleteAuthor() throws Exception{
	
		authorDao.delete(1L);
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(AUTHOR_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_DELETE_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(AUTHOR_TABLE);
		
		Assertion.assertEquals(expectedlTable, actualTable);
	}
	
	/**
	 * Tests read all operation.
	 * @throws Exception
	 */
	@Test
	public void getAllAuthors() throws Exception{

		List<Author> authorsList = authorDao.readAll();
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(AUTHOR_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_GET_ALL_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(AUTHOR_TABLE);
		
		Assert.assertEquals(expectedlTable.getRowCount(), authorsList.size());
		Assertion.assertEquals(expectedlTable, actualTable);		
	}
	
}
