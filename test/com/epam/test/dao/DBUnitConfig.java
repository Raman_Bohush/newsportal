package com.epam.test.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Configuration class for DBUnit tests.
 * @author Roma
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:Beans.xml"})
public class DBUnitConfig extends DBTestCase{
	
	private final static Logger logger = Logger.getLogger(DBUnitConfig.class);
	
	private static final String JDBC_DRIVER = "jdbc.driver";
	private static final String JDBC_URL = "jdbc.url";
	private static final String USER = "jdbc.user";
	private static final String PASSWORD = "jdbc.password";
	
	final static String DATABASE_DATASET_PATH = "resources/test/databaseDataset.xml";
	final static String EXPECTED_ADD_FOR_NEWS_DATASET_PATH = "resources/test/expectedAddForNewsDataset.xml";
	final static String EXPECTED_DELETE_DATASET_PATH = "resources/test/expectedDeleteDataset.xml";
	final static String EXPECTED_GET_ALL_DATASET_PATH = "resources/test/expectedGetAllDataset.xml";
	final static String EXPECTED_SEARCH_NEWS_BY_AUTHOR_DATASET_PATH = "resources/test/expectedSearchNewsByAuthorDataset.xml";
	final static String EXPECTED_SEARCH_NEWS_BY_TAG_DATASET_PATH = "resources/test/expectedSearchNewsByTagDataset.xml";
	final static String EXPECTED_UPDATE_DATASET_PATH = "resources/test/expectedUpdateDataset.xml";
	
	/**
	 * Database tester.
	 */
	protected IDatabaseTester tester;
	
	/**
	 * Dataset before to each test.
	 */
	protected IDataSet databaseDataset;
	
	/**
	 * Properties of the database.
	 */
	private Properties prop = new Properties();;
	
	public DBUnitConfig() {
		
		super();
		try {
			prop.load(new FileInputStream("Resources/db.properties"));
		} catch (FileNotFoundException e) {
			logger.error("File is not found.", e);
		} catch (IOException e) {
			logger.error("IO error", e);
		}
	}
	
	/**
	 * Initializes the tester before to each test
	 */
	@Before
    public void setUp() throws Exception {
		Locale.setDefault(Locale.ENGLISH);
        tester = new JdbcDatabaseTester(prop.getProperty(JDBC_DRIVER), prop.getProperty(JDBC_URL), 
        		prop.getProperty(USER), prop.getProperty(PASSWORD));
    }
	
	/**
	 * Gets dataset.
	 */
	@Override
    protected IDataSet getDataSet() throws Exception {
        return databaseDataset;
    }
 
	/**
	 * Clears database tables after test.
	 */
    @Override
    protected DatabaseOperation getTearDownOperation() throws Exception {
        return DatabaseOperation.DELETE_ALL;
    }

}
