package com.epam.test.dao;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.dao.impl.NewsDao;
import com.epam.domain.Author;
import com.epam.domain.News;
import com.epam.domain.Tag;

/**
 * Tests News Data Access Object using DBUnit framework.
 * @author Roma
 *
 */
public class NewsDaoTest extends DBUnitConfig{
	
	private final static String NEWS_TABLE = "news";
	private final static String NEWS_AUTHOR_TABLE = "news_author";
	private final static String NEWS_TAG_TABLE = "news_tag";
	
	/**
	 * News Data Access Object.
	 */
	@Autowired
	private NewsDao newsDao;
	
	@Before
	@Override
	public void setUp() throws Exception {

		super.setUp();
		databaseDataset = new FlatXmlDataSetBuilder().build(new File(DATABASE_DATASET_PATH));
		tester.setDataSet(databaseDataset);
        tester.onSetup();
	}
	
	/**
	 * Tests create operation.
	 * @throws Exception
	 */
	@Test
	public void createNews() throws Exception{
		
		News news = new News();
		news.setTitle("Windows");
		news.setShortText("short4");
		news.setFullText("full4");
		news.setCreationDate(Timestamp.valueOf("2015-08-17 00:00:00"));
		news.setModificationDate(new SimpleDateFormat("yyyy-MM-dd").parse("2015-09-17"));
		
		long id = newsDao.create(news);
		Assert.assertEquals(news.getTitle(), newsDao.read(id).getTitle());
		Assert.assertEquals(news.getShortText(), newsDao.read(id).getShortText());
		Assert.assertEquals(news.getFullText(), newsDao.read(id).getFullText());
		Assert.assertEquals(news.getCreationDate(), newsDao.read(id).getCreationDate());
		Assert.assertEquals(news.getModificationDate(), newsDao.read(id).getModificationDate());
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(NEWS_TABLE);		
        Assert.assertEquals(4, actualTable.getRowCount());
		
	}
	
	/**
	 * Tests read by id operation.
	 * @throws Exception
	 */
	@Test
	public void readNewsById() throws Exception{
		
		News news = newsDao.read(2L);
		
		Assert.assertEquals("Programming", news.getTitle());		
	}
	
	/**
	 * Tests update operation.
	 * @throws Exception
	 */
	@Test
	public void updateNews() throws Exception{
		
		News news = new News();
		news.setNewsId(3L);
		news.setTitle("Belarus Minsk");
		news.setShortText("short3");
		news.setFullText("full3");
		news.setCreationDate(Timestamp.valueOf("2015-04-17 11:11:11"));
		news.setModificationDate(new SimpleDateFormat("yyyy-MM-dd").parse("2015-09-17"));
		newsDao.update(news);
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(NEWS_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_UPDATE_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(NEWS_TABLE);
		
		Assertion.assertEquals(expectedlTable, actualTable);
		
	}
	
	/**
	 * Tests delete operation.
	 * @throws Exception
	 */
	@Test
	public void deleteNews() throws Exception{
		
		newsDao.delete(2L);
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(NEWS_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_DELETE_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(NEWS_TABLE);
		
		Assertion.assertEquals(expectedlTable, actualTable);
	}
	
	/**
	 * Tests read all operation.
	 * @throws Exception
	 */
	@Test
	public void getAllNews() throws Exception{
		
		List<News> authorsList = newsDao.readAll();
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(NEWS_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_GET_ALL_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(NEWS_TABLE);
		
		Assert.assertEquals(expectedlTable.getRowCount(), authorsList.size());
		Assertion.assertEquals(expectedlTable, actualTable);		
	}
	
	/**
	 * Tests search news by author operation.
	 * @throws Exception
	 */
	@Test
	public void searchNewsByAuthor() throws Exception{
		
		Author author = new Author();
		author.setAuthorId(1L);
		
		List<News> newsList = newsDao.searchNewsByAuthor(author);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_SEARCH_NEWS_BY_AUTHOR_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(NEWS_TABLE);
		
		Assert.assertEquals(expectedlTable.getRowCount(), newsList.size());
		
	}
	
	/**
	 * Tests search news by tag operation.
	 * @throws Exception
	 */
	@Test
	public void searchNewsByTag() throws Exception{
		
		Tag tag = new Tag();
		tag.setTagId(2L);
		
		List<News> newsList = newsDao.searchNewsByTag(tag);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_SEARCH_NEWS_BY_TAG_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(NEWS_TABLE);
		
		Assert.assertEquals(expectedlTable.getRowCount(), newsList.size());
		
	}
	
	/**
	 * Tests sorting news by comments operation.
	 * @throws Exception
	 */
	@Test
	public void getSortedNews() throws Exception{
		
		List<News> newsList = newsDao.getSortedNews();
		
		Assert.assertEquals(3, newsList.size());
		
		Assert.assertEquals("Programming", newsList.get(0).getTitle());
		Assert.assertEquals("Football", newsList.get(1).getTitle());
		Assert.assertEquals("Belarus", newsList.get(2).getTitle());
			
	}
	
	/**
	 * Tests add author for news operation.
	 * @throws Exception
	 */
	@Test
	public void addAuthorForNews() throws Exception{
		
		newsDao.addAuthorForNews(4L, 4L);
		
		ITable actualTable = tester.getConnection().createQueryTable(NEWS_AUTHOR_TABLE, "SELECT news_id, author_id FROM news_author ORDER BY news_id");
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_ADD_FOR_NEWS_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(NEWS_AUTHOR_TABLE);
		
		Assert.assertEquals(expectedlTable.getRowCount(), actualTable.getRowCount());
		Assertion.assertEquals(expectedlTable, actualTable);
		
	}
	
	/**
	 * Tests add tag for news operation.
	 * @throws Exception
	 */
	@Test
	public void addTagForNews() throws Exception{
		
		newsDao.addTagForNews(4L, 4L);
		
		ITable actualTable = tester.getConnection().createQueryTable(NEWS_TAG_TABLE, "SELECT news_id, tag_id FROM news_tag ORDER BY news_id, tag_id");
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_ADD_FOR_NEWS_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(NEWS_TAG_TABLE);
		
		Assert.assertEquals(expectedlTable.getRowCount(), actualTable.getRowCount());
		Assertion.assertEquals(expectedlTable, actualTable);
		
	}

}
