package com.epam.test.dao;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.dao.impl.CommentDao;
import com.epam.domain.Comment;

/**
 * Tests Comment Data Access Object using DBUnit framework.
 * @author Roma
 *
 */
public class CommentDaoTest extends DBUnitConfig{

	private final static String COMMENTS_TABLE = "comments";
	
	/**
	 * Comment Data Access Object.
	 */
	@Autowired
	private CommentDao commentDao;
	
	@Before
	@Override
	public void setUp() throws Exception {

		super.setUp();
		databaseDataset = new FlatXmlDataSetBuilder().build(new File(DATABASE_DATASET_PATH));
		tester.setDataSet(databaseDataset);
        tester.onSetup();
	}
	
	/**
	 * Tests create operation.
	 * @throws Exception
	 */
	@Test
	public void createComment() throws Exception{
		
		Comment comment = new Comment();
		comment.setNewsId(1L);
		comment.setCommentText("big com_text1");
		comment.setCreationDate(Timestamp.valueOf("2015-08-17 00:00:00"));
		
		long id = commentDao.create(comment);
		Assert.assertEquals(comment.getNewsId(), commentDao.read(id).getNewsId());
		Assert.assertEquals(comment.getCommentText(), commentDao.read(id).getCommentText());
		Assert.assertEquals(comment.getCreationDate(), commentDao.read(id).getCreationDate());
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(COMMENTS_TABLE);		
        Assert.assertEquals(4, actualTable.getRowCount());
	}
	
	/**
	 * Tests read by id operation.
	 * @throws Exception
	 */
	@Test
	public void readCommentById() throws Exception{
		
		Comment comment = commentDao.read(2L);
		
		Assert.assertEquals("com_text2", comment.getCommentText());		
	}
	
	/**
	 * Tests update operation.
	 * @throws Exception
	 */
	@Test
	public void updateComment() throws Exception{
		
		Comment comment = new Comment();
		comment.setCommentId(1L);
		comment.setNewsId(1L);
		comment.setCommentText("big com_text1");
		comment.setCreationDate(Timestamp.valueOf("2015-08-17 00:00:00"));
		commentDao.update(comment);
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(COMMENTS_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_UPDATE_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(COMMENTS_TABLE);
		
		Assertion.assertEquals(expectedlTable, actualTable);
		
	}
	
	/**
	 * Tests delete operation.
	 * @throws Exception
	 */
	@Test
	public void deleteComment() throws Exception{

		commentDao.delete(1L);
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(COMMENTS_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_DELETE_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(COMMENTS_TABLE);
		
		Assertion.assertEquals(expectedlTable, actualTable);
	}
	
	/**
	 * Tests read all operation.
	 * @throws Exception
	 */
	@Test
	public void getAllComments() throws Exception{
		
		List<Comment> commentsList = commentDao.readAll();
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(COMMENTS_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_GET_ALL_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(COMMENTS_TABLE);
		
		Assert.assertEquals(expectedlTable.getRowCount(), commentsList.size());
		Assertion.assertEquals(expectedlTable, actualTable);		
	}
}
