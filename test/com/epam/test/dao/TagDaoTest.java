package com.epam.test.dao;

import java.io.File;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;




import com.epam.dao.impl.TagDao;
import com.epam.domain.Tag;

/**
 * Tests Tag Data Access Object using DBUnit framework.
 * @author Roma
 *
 */
public class TagDaoTest extends DBUnitConfig{
	
	private final static String TAG_TABLE = "tag";
	
	/**
	 * Tag Data Access Object.
	 */
	@Autowired
	private TagDao tagDao;
	
	@Before
	@Override
	public void setUp() throws Exception {

		super.setUp();
		databaseDataset = new FlatXmlDataSetBuilder().build(new File(DATABASE_DATASET_PATH));
		tester.setDataSet(databaseDataset);
        tester.onSetup();
	}
	
	/**
	 * Tests create operation.
	 * @throws Exception
	 */
	@Test
	public void createTag() throws Exception{
		
		Tag tag = new Tag();
		tag.setTagName("IT");
		
		long id = tagDao.create(tag);
		Assert.assertEquals(tag.getTagName(), tagDao.read(id).getTagName());
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(TAG_TABLE);		
        Assert.assertEquals(4, actualTable.getRowCount());
		
	}
	
	/**
	 * Tests read by id operation.
	 * @throws Exception
	 */
	@Test
	public void readTagById() throws Exception{
		
		Tag tag = tagDao.read(2L);
		
		Assert.assertEquals("Java", tag.getTagName());		
	}
	
	/**
	 * Tests update operation.
	 * @throws Exception
	 */
	@Test
	public void updateTag() throws Exception{
		
		Tag tag = new Tag();
		tag.setTagId(2L);
		tag.setTagName("Java EE");
		tagDao.update(tag);
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(TAG_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_UPDATE_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(TAG_TABLE);
		
		Assertion.assertEquals(expectedlTable, actualTable);
		
	}

	/**
	 * Tests delete operation.
	 * @throws Exception
	 */
	@Test
	public void deleteTag() throws Exception{
		
		tagDao.delete(1L);
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(TAG_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_DELETE_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(TAG_TABLE);
		
		Assertion.assertEquals(expectedlTable, actualTable);
	}
	
	/**
	 * Tests read all operation.
	 * @throws Exception
	 */
	@Test
	public void getAllTags() throws Exception{
		
		List<Tag> tagsList = tagDao.readAll();
		
		IDataSet actualDataset = tester.getConnection().createDataSet();
		ITable actualTable = actualDataset.getTable(TAG_TABLE);
		
		IDataSet expectedDataset = new FlatXmlDataSetBuilder().build(new File(EXPECTED_GET_ALL_DATASET_PATH));
		ITable expectedlTable = expectedDataset.getTable(TAG_TABLE);
		
		Assert.assertEquals(expectedlTable.getRowCount(), tagsList.size());
		Assertion.assertEquals(expectedlTable, actualTable);		
	}
	
}
