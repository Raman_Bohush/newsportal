package com.epam.test.service;

import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.epam.dao.exception.DaoException;
import com.epam.dao.impl.TagDao;
import com.epam.domain.Tag;
import com.epam.service.exception.ServiceException;
import com.epam.service.impl.TagService;

/**
 * Tests Tag Service using mocking framework - Mockito.
 * @author Roma
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {
	
	/**
	 * Tag Data Access Object is mock.
	 */
	@Mock
	private TagDao tagDao;
	
	/**
	 * Tag Service.
	 */
	@InjectMocks
	private TagService tagService;
	
	/**
	 * Tests create operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void createTag() throws ServiceException, DaoException{
		
		Tag tag = new Tag();
		tag.setTagName("taGGG");
		
		when(tagDao.create(tag)).thenReturn(3L);
		
		Assert.assertEquals(3, tagService.create(tag));
		
		verify(tagDao, times(1)).create(tag);
	}
	
	/**
	 * Tests read by id operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readTag() throws ServiceException, DaoException{
		
		Tag tag = new Tag();
		tag.setTagName("tag");
		
		when(tagDao.read(1L)).thenReturn(tag);
		
		Assert.assertEquals(tag, tagService.read(1L));
		
		verify(tagDao, times(1)).read(1L);
	}
	
	/**
	 * Tests update operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void updateTag() throws ServiceException, DaoException{
		
		Tag tag = new Tag();
		tag.setTagName("taGGG");
		
		tagService.update(tag);
		verify(tagDao).update(tag);
	}
	
	/**
	 * Tests delete operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void deleteTag() throws ServiceException, DaoException{

		tagService.delete(1L);
		verify(tagDao).delete(1L);
	}
	
	/**
	 * Tests read all operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readAllTags() throws ServiceException, DaoException{
		
		List<Tag> tagsList = new ArrayList<>();
		Tag tag1 = new Tag();
		tag1.setTagName("tag1");
		Tag tag2 = new Tag();
		tag2.setTagName("tag2");
		tagsList.add(tag1);
		tagsList.add(tag2);
		
		when(tagDao.readAll()).thenReturn(tagsList);
		
		Assert.assertEquals(tagsList.size(), tagService.readAll().size());
		Assert.assertEquals(tagsList, tagService.readAll());
		
		verify(tagDao, times(2)).readAll();
	}
}
