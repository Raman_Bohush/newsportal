package com.epam.test.service;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
import com.epam.dao.exception.DaoException;
import com.epam.dao.impl.CommentDao;
import com.epam.domain.Comment;
import com.epam.service.exception.ServiceException;
import com.epam.service.impl.CommentService;

/**
 * Tests Comment Service using mocking framework - Mockito.
 * @author Roma
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
	
	/**
	 * Comment Data Access Object is mock.
	 */
	@Mock
	private CommentDao commentDao;
	
	/**
	 * Comment Service.
	 */
	@InjectMocks
	private CommentService commentService;
	
	/**
	 * Tests create operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void createComment() throws ServiceException, DaoException{
		
		Comment comment = new Comment();
		comment.setCommentText("text");
		
		when(commentDao.create(comment)).thenReturn(2L);
		
		Assert.assertEquals(2, commentService.create(comment));
		
		verify(commentDao, times(1)).create(comment);
	}
	
	/**
	 * Tests read by id operation.
	 * @throws DaoException
	 * @throws ServiceException
	 */
	@Test
	public void readComment() throws ServiceException, DaoException{
		
		Comment comment = new Comment();
		comment.setCommentText("text");
		
		when(commentDao.read(1L)).thenReturn(comment);
		
		Assert.assertEquals(comment, commentService.read(1L));
		
		verify(commentDao, times(1)).read(1L);
	}
	
	/**
	 * Tests update operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void updateComment() throws ServiceException, DaoException{
		
		Comment comment = new Comment();
		comment.setCommentText("text");
		
		commentService.update(comment);
		verify(commentDao).update(comment);
	}
	
	/**
	 * Tests delete operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void deleteComment() throws ServiceException, DaoException{

		commentService.delete(2L);
		verify(commentDao).delete(2L);
	}
	
	/**
	 * Tests read all operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readAllComments() throws ServiceException, DaoException{
		
		List<Comment> commentsList = new ArrayList<>();
		Comment comment1 = new Comment();
		comment1.setCommentText("text1");
		Comment comment2 = new Comment();
		comment2.setCommentText("text2");
		commentsList.add(comment1);
		commentsList.add(comment2);
		
		when(commentDao.readAll()).thenReturn(commentsList);
		
		Assert.assertEquals(commentsList.size(), commentService.readAll().size());
		Assert.assertEquals(commentsList, commentService.readAll());

		verify(commentDao, times(2)).readAll();
	}
}
