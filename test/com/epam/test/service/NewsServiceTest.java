package com.epam.test.service;

import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.epam.dao.exception.DaoException;
import com.epam.dao.impl.NewsDao;
import com.epam.domain.Author;
import com.epam.domain.News;
import com.epam.domain.Tag;
import com.epam.service.exception.ServiceException;
import com.epam.service.impl.NewsService;

/**
 * Tests News Service using mocking framework - Mockito.
 * @author Roma
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {
	
	/**
	 * News Data Access Object is mock.
	 */
	@Mock
	private NewsDao newsDao;
	
	/**
	 * News Service.
	 */
	@InjectMocks
	private NewsService newsService;
	
	/**
	 * Tests create operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void createNews() throws ServiceException, DaoException{
		
		News newNews = new News();
		newNews.setTitle("title");
		
		when(newsDao.create(newNews)).thenReturn(2L);

		Assert.assertEquals(2, newsService.create(newNews));
		verify(newsDao, times(1)).create(newNews);
	}
	
	/**
	 * Tests read by id operation.
	 * @throws DaoException
	 * @throws ServiceException
	 */
	@Test
	public void readNews() throws ServiceException, DaoException{
		
		News newNews = new News();
		newNews.setTitle("title");
		
		when(newsDao.read(3L)).thenReturn(newNews);
		
		Assert.assertEquals(newNews, newsService.read(3L));
		
		verify(newsDao, times(1)).read(3L);
	}
	
	/**
	 * Tests update operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void updateNews() throws ServiceException, DaoException{
		
		News newNews = new News();
		newNews.setTitle("title");
		
		newsService.update(newNews);
		verify(newsDao).update(newNews);
	}
	
	/**
	 * Tests delete operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void deleteNews() throws ServiceException, DaoException{

		newsService.delete(2L);
		verify(newsDao).delete(2L);
	}
	
	/**
	 * Tests read all operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readAllNews() throws ServiceException, DaoException{
		
		List<News> newsList = new ArrayList<>();
		News news1 = new News();
		news1.setTitle("title1");
		News news2 = new News();
		news2.setTitle("title2");
		newsList.add(news1);
		newsList.add(news2);
		
		when(newsDao.readAll()).thenReturn(newsList);
		
		Assert.assertEquals(newsList.size(), newsService.readAll().size());
		Assert.assertEquals(newsList, newsService.readAll());

		verify(newsDao, times(2)).readAll();
	}
	
	/**
	 * Tests add author for news operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void addAuthorForNews() throws ServiceException, DaoException{
		
		newsService.addAuthorForNews(4L, 4L);
		verify(newsDao).addAuthorForNews(4L, 4L);
	}
	
	/**
	 * Tests add tag for news operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void addTagForNews() throws ServiceException, DaoException{
		
		newsService.addTagForNews(4L, 4L);
		verify(newsDao).addTagForNews(4L, 4L);
	}
	
	/**
	 * Tests search news by author operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void searchNewsByAuthor() throws ServiceException, DaoException{
		
		Author author = new Author();
		author.setName("roma");
		List<News> newsList = new ArrayList<>();
		News news1 = new News();
		news1.setTitle("title1");
		News news2 = new News();
		news2.setTitle("title2");
		newsList.add(news1);
		newsList.add(news2);
		
		when(newsDao.searchNewsByAuthor(author)).thenReturn(newsList);
		
		Assert.assertEquals(newsList.size(), newsService.searchNewsByAuthor(author).size());
		Assert.assertEquals(newsList, newsService.searchNewsByAuthor(author));
		
		verify(newsDao, times(2)).searchNewsByAuthor(author);
	}
	
	/**
	 * Tests search news by tag operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void searchNewsByTag() throws ServiceException, DaoException{
		
		Tag tag = new Tag();
		tag.setTagName("cars");
		List<News> newsList = new ArrayList<>();
		News news1 = new News();
		news1.setTitle("title1");
		News news2 = new News();
		news2.setTitle("title2");
		newsList.add(news1);
		newsList.add(news2);
		
		when(newsDao.searchNewsByTag(tag)).thenReturn(newsList);
		
		Assert.assertEquals(newsList.size(), newsService.searchNewsByTag(tag).size());
		Assert.assertEquals(newsList, newsService.searchNewsByTag(tag));
		
		verify(newsDao, times(2)).searchNewsByTag(tag);
	}
	
	/**
	 * Tests sorting news by comment operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	public void getSortedNews() throws ServiceException, DaoException{
		
		List<News> newsList = new ArrayList<>();
		News news1 = new News();
		news1.setTitle("title1");
		News news2 = new News();
		news2.setTitle("title2");
		newsList.add(news1);
		newsList.add(news2);
		
		when(newsDao.readAll()).thenReturn(newsList);
		
		Assert.assertEquals(newsList.size(), newsService.readAll().size());
		Assert.assertEquals(newsList, newsService.readAll());

		verify(newsDao, times(2)).readAll();
	}
}
