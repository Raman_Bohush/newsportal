package com.epam.test.service;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;
import com.epam.dao.exception.DaoException;
import com.epam.dao.impl.AuthorDao;
import com.epam.domain.Author;
import com.epam.service.exception.ServiceException;
import com.epam.service.impl.AuthorService;

/**
 * Tests Author Service using mocking framework - Mockito.
 * @author Roma
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {
	
	/**
	 * Author Data Access Object is mock.
	 */
	@Mock
	private AuthorDao authorDao;
	
	/**
	 * Author Service.
	 */
	@InjectMocks
	private AuthorService authorService;
	
	/**
	 * Tests create operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void createAuthor() throws ServiceException, DaoException{
		
		Author author = new Author();
		author.setName("name");
		
		when(authorDao.create(author)).thenReturn(3L);

		Assert.assertEquals(3, authorService.create(author));
		verify(authorDao, times(1)).create(author);
	}
	
	/**
	 * Tests read by id operation.
	 * @throws DaoException
	 * @throws ServiceException
	 */
	@Test
	public void readAuthor() throws DaoException, ServiceException{
		
		Author author = new Author();
		author.setName("name");
		
		when(authorDao.read(1L)).thenReturn(author);
		Assert.assertEquals(author, authorService.read(1L));
		
		verify(authorDao, times(1)).read(1L);
	}
	
	/**
	 * Tests update operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void updateAuthor() throws ServiceException, DaoException{
		
		Author author = new Author();
		author.setName("name");
		
		authorService.update(author);
		verify(authorDao, times(1)).update(author);
	}
	
	/**
	 * Tests delete operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void deleteAuthor() throws ServiceException, DaoException{

		authorService.delete(2L);
		verify(authorDao, times(1)).delete(2L);
	}
	
	/**
	 * Tests read all operation.
	 * @throws ServiceException
	 * @throws DaoException
	 */
	@Test
	public void readAllAuthors() throws DaoException, ServiceException{
		
		List<Author> authorsList = new ArrayList<>();
		Author author1 = new Author();
		author1.setName("roma");
		Author author2 = new Author();
		author2.setName("lena");
 		authorsList.add(author1);
		authorsList.add(author2);
		
		when(authorDao.readAll()).thenReturn(authorsList);
		
		Assert.assertEquals(authorsList.size(), authorService.readAll().size());
		Assert.assertEquals(authorsList, authorService.readAll());
		
		verify(authorDao, times(2)).readAll();
		

	}
}
